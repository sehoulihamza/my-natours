const viewController = require('./../controllers/viewController');
const authController = require('./../controllers/authController');
const bookingController = require('./../controllers/bookingController');
const express = require('express');

const router = express.Router();
router.get('/myTours', authController.protect, viewController.getMyTours);
router.get('/me', authController.protect, viewController.me);
router.get(
  '/',
  bookingController.createBooking,
  authController.isLoggedIn,
  viewController.getOverview
);
router.use(authController.isLoggedIn);
router.get('/login', viewController.login);
router.get('/tour/:tourSlug', viewController.getTourView);

module.exports = router;
