const mongoose = require('mongoose');

const bookingSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  },
  tour: {
    type: mongoose.Schema.ObjectId,
    ref: 'Tour'
  },
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  bookedAt: {
    type: Date,
    default: Date.now()
  },
  paid: {
    type: Boolean,
    default: true
  }
});

// bookingSchema.pre(/^find/, function(next) {
//   this.populate('user').populate({
//     path: 'tour',
//     select: 'name'
//   });
// });
//hi
const Booking = mongoose.model('Booking', bookingSchema);

module.exports = Booking;
