const locations = JSON.parse(document.getElementById('map').dataset.locations);

mapboxgl.accessToken =
  'pk.eyJ1IjoiaGFtemFzZWhvdWxpIiwiYSI6ImNrbXV6azhsOTAwOWcydW52aHZ3eTdvemgifQ.gmuXdOIcVGBxkQk0Mt9d_g';
let map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/hamzasehouli/ckmv0849j02f217o3svejrjff'
});

const coords = locations.map(el => el.coordinates);
let bounds = new mapboxgl.LngLatBounds();

locations.forEach(async loc => {
  new mapboxgl.Marker().setLngLat(loc.coordinates).addTo(map);
  new mapboxgl.Popup({
    offset: 30
  })
    .setLngLat(loc.coordinates)
    .setHTML(`<p>${loc.day} : ${loc.description}</p>`)
    .addTo(map);
  bounds.extend(loc.coordinates);
});

map.fitBounds(bounds, {
  padding: {
    left: 100,
    bottom: 200,
    right: 100,
    top: 250
  }
});
