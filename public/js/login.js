'use strict';
const stripe = Stripe(
  'pk_test_51IjRszHbBOArn4U0TE6UCYmju4q4tKjHUmjnPoEO02qsUaELHtMcsd5nSgSF3nkjK8UXVwmtRCSRH93XMSdPzv2g00B1SOnfDN'
);
const email = document.getElementById('email');
const password = document.getElementById('password');
const loginBtn = document.querySelector('.btn');
const form = document.querySelector('.form--login');
const btnLogout = document.querySelector('.nav__el--logout');
const formBtn = document.querySelector('.btn--me');
const btnPassword = document.querySelector('.btn--password');
const fullName = document.getElementById('name');
const formMe = document.querySelector('.form-user-data');
const formPassword = document.querySelector('.form-user-settings');
const currentPassword = document.getElementById('password-current');
const Password = document.getElementById('password');
const photo = document.getElementById('upload-photo');
const confirmPassword = document.getElementById('password-confirm');
const stripeBtn = document.getElementById('stripeBtn');

form?.addEventListener('submit', async e => {
  e.preventDefault();
  const data = {
    email: email.value,
    password: password.value
  };
  const res = await fetch(`/api/v1/users/login`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  const response = await res.json();

  if (response.status === 'success') {
    const successDiv = document.createElement('div');
    successDiv.innerText = 'successfully login';
    successDiv.className = 'alert alert--success';

    document
      .querySelector('.body')
      .insertAdjacentElement('afterbegin', successDiv);

    setTimeout(() => {
      successDiv.closest('.body').removeChild(successDiv);
      location.assign('/');
    }, 2000);
  } else if (response.status === 'fail') {
    const errorDiv = document.createElement('div');
    errorDiv.innerText = 'login faild, please enter valid email and password';
    errorDiv.className = 'alert alert--error';

    document
      .querySelector('.body')
      .insertAdjacentElement('afterbegin', errorDiv);
    setTimeout(() => {
      errorDiv.closest('.body').removeChild(errorDiv);
    }, 2500);
  }
});

btnLogout?.addEventListener('click', async () => {
  const res = await fetch(`/api/v1/users/logout`, {
    method: 'GET'
  });
  const response = await res.json();

  if (response.status === 'success') {
    location.reload(true);
  }
});

formMe?.addEventListener('submit', async e => {
  e.preventDefault();
  formBtn.textContent = '...updating';
  const formData = new FormData();
  formData.append('photo', photo.files[0]);
  formData.append('email', email.value);
  formData.append('name', fullName.value);

  const res = await fetch('/api/v1/users/updateMe', {
    method: 'PATCH',
    body: formData
  });
  const response = await res.json();
  formBtn.textContent = 'Save settings';
  if (response.status === 'success') {
    const successDiv = document.createElement('div');
    successDiv.innerText = 'successfully updated';
    successDiv.className = 'alert alert--success';

    document
      .querySelector('.body')
      .insertAdjacentElement('afterbegin', successDiv);

    setTimeout(() => {
      successDiv.closest('.body').removeChild(successDiv);
      // location.reload(true);
    }, 500);
  } else {
    const errorDiv = document.createElement('div');
    errorDiv.innerText = response.message;
    errorDiv.className = 'alert alert--error';

    document
      .querySelector('.body')
      .insertAdjacentElement('afterbegin', errorDiv);
    setTimeout(() => {
      errorDiv.closest('.body').removeChild(errorDiv);
    }, 2500);
  }
});

formPassword?.addEventListener('submit', async e => {
  e.preventDefault();
  const data = {
    passwordCurrent: currentPassword.value,
    password: password.value,
    passwordConfirm: confirmPassword.value
  };
  const res = await fetch('/api/v1/users/updateMyPassword', {
    method: 'PATCH',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  const response = await res.json();
});

stripeBtn?.addEventListener('click', async e => {
  try {
    const tourId = e.target.dataset.tourId;
    const res = await fetch(`/api/v1/bookings/checkout/${tourId}`, {
      method: 'GET'
    });
    const session = await res.json();

    await stripe.redirectToCheckout({
      sessionId: session.session.id
    });
  } catch (err) {
    const errorDiv = document.createElement('div');
    errorDiv.innerText = err;
    errorDiv.className = 'alert alert--error';

    document
      .querySelector('.body')
      .insertAdjacentElement('afterbegin', errorDiv);
  }
});

/* fetch('/create-checkout-session', {
  method: 'POST',
})
.then(function(response) {
  return response.json();
})
.then(function(session) {
  return stripe.redirectToCheckout({ sessionId: session.id });
})
.then(function(result) {
  // If `redirectToCheckout` fails due to a browser or network
  // error, you should display the localized error message to your
  // customer using `error.message`.
  if (result.error) {
    alert(result.error.message);
  }
}); */
