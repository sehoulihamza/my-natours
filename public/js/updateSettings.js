'use strict';

const formBtn = document.querySelector('.btn--me');
const email = document.getElementById('email');
const fullName = document.getElementById('name');

formBtn?.addEventListener('submit', async e => {
  e.preventDefault();

  const data = {
    email: email.value,
    name: fullName.value
  };
  const res = await fetch('/api/v1/users/updateMe', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  const response = await res.json();
  
});
