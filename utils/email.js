const nodemailer = require('nodemailer');
const nodemailerSendgrid = require('nodemailer-sendgrid');
const pug = require('pug');
const { htmlToText } = require('html-to-text');

module.exports = class Email {
  constructor(user, url) {
    this.from = 'Hamza Sehouli <sehouli.hamza@gmail.com>';
    this.to = user.email;
    this.url = url;
    this.firstName = user.name.split(' ')[0];
  }

  newTransporter() {
    if (process.env.NODE_ENV === 'production') {
      return nodemailer.createTransport(
        nodemailerSendgrid({
          apiKey: process.env.SENDGRID_PASSWORD
        })
      );
    }

    return nodemailer.createTransport({
      host: 'smtp.mailtrap.io',
      port: 2525,
      auth: {
        user: '33cbbfabacc4d6',
        pass: 'fee0765f92eeb2'
      }
    });
  }

  async send(template, subject) {
    const html = pug.renderFile(template, {
      firstName: this.firstName,
      url: this.url,
      subject
    });
    const text = await htmlToText(html, {
      wordwrap: 130
    });
    const mailOptions = {
      from: this.from,
      to: this.to,
      subject,
      html,
      text
    };
    await this.newTransporter().sendMail(mailOptions);
  }

  async welcome() {
    await this.send(
      `${__dirname}/../views/welcome.pug`,
      'welcome to natours family'
    );
  }

  async sendResetPassword() {
    await this.send(
      `${__dirname}/../views/resetPassword.pug`,
      'click on this link to reset your password (valid only for 10min)'
    );
  }
};

// const sendEmail = async options => {
//   // 1) Create a transporter
//   const transporter = nodemailer.createTransport({
//     host: process.env.EMAIL_HOST,
//     port: process.env.EMAIL_PORT,
//     auth: {
//       user: process.env.EMAIL_USERNAME,
//       pass: process.env.EMAIL_PASSWORD
//     }
//   });

//   // 2) Define the email options
//   const mailOptions = {
//     from: 'Jonas Schmedtmann <hello@jonas.io>',
//     to: options.email,
//     subject: options.subject,
//     text: options.message
//     // html:
//   };

//   // 3) Actually send the email
//   await transporter.sendMail(mailOptions);
// };

// module.exports = sendEmail;
