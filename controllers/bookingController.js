const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const Tour = require('./../models/tourModel');
const Booking = require('./../models/bookingModel');
const catchAsync = require('./../utils/catchAsync');
const factory = require('./handlerFactory');
const AppError = require('./../utils/appError');

exports.createSession = catchAsync(async (req, res, next) => {
  //1) get to purchase tour
  const tour = await Tour.findById(req.params.tourId);

  //2) create session
  const session = await stripe.checkout.sessions.create({
    success_url: `${req.protocol}://${req.get('host')}?tour=${tour.id}&user=${
      req.user.id
    }&price=${tour.price}&name=${tour.slug}`,
    cancel_url: `${req.protocol}://${req.get('host')}`,
    payment_method_types: ['card'],
    customer_email: req.user.email,
    client_reference_id: req.params.tourId,
    line_items: [
      {
        name: tour.name,
        amount: tour.price * 100,
        quantity: 1,
        description: tour.summary,
        images: ['https://www.natours.dev/img/tours/tour-5-cover.jpg'],
        currency: 'usd'
      }
    ]
  });
  console.log(session);
  //3) send the session to the front end
  res.status(200).json({
    status: 'success',
    session
  });
});

exports.createBooking = catchAsync(async (req, res, next) => {
  console.log(req.query);
  const { tour, user, price, name } = req.query;
  if (!tour || !user || !price || !name) return next();
  await Booking.create({ tour, user, price, name });
  res.redirect(`${req.protocol}://${req.get('host')}`);
});
