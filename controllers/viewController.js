const jwt = require('jsonwebtoken');
const catchAsync = require('./../utils/catchAsync');
const Tour = require('./../models/tourModel');
const Booking = require('./../models/bookingModel');
const AppError = require('./../utils/appError');
const User = require('./../models/userModel');

exports.getOverview = catchAsync(async (req, res, next) => {
  const tours = await Tour.find();

  res.status(200).render('overview', {
    title: 'All tours',
    tours
  });
});

exports.getTourView = catchAsync(async (req, res, next) => {
  const tour = await await Tour.findOne({
    slug: req.params.tourSlug
  }).populate('reviews');
  if (!tour) return next(new AppError('there is no tour with that name', 404));
  res.status(200).render('tour', {
    title: `${tour?.name}`,
    tour
  });
});

exports.login = catchAsync(async (req, res, next) => {
  res.status(200).render('login', {
    title: 'login'
  });
});

exports.me = catchAsync(async (req, res, next) => {
  console.log(res.locals.user);
  if (!res.locals.user)
    return next(new AppError('you are not logged in , please login', 403));
  res.status(200).render('me', {
    title: 'Settings'
  });
});
exports.getMyTours = catchAsync(async (req, res, next) => {
  const myBookings = await Booking.find({ user: req.user.id });
  const myTours = myBookings.map(t => t.tour);
  const [tours] = await Promise.all(
    myTours.map(async t => await Tour.find({ _id: t }))
  );
  console.log(tours);
  console.log(myBookings);
  res.status(200).render('overview', {
    title: 'My tours',
    tours
  });
});
